<?php
define('IMAGE_360'      , '360 image');
define('VIDEO_360'      , '360 video');
define('ANNUAL_REPORT'  , 'Annual report');
define('BROCHURE'       , 'Brochure');
define('BULLETIN'       , 'Bulletin');
define('CDS_360_IMAGE'  , 'CDS 360 image');
define('CDS_360_VIDEO'  , 'CDS 360 video');
define('CDS_IMAGE'      , 'CDS Image');
define('CDS_VIDEO'      , 'CDS Video');
define('COURIER'        , 'Courier');
define('DOCUMENT'       , 'Document');
define('IMAGE'          , 'Image');
define('PRESENTATION'   , 'Presentation');
define('PUBLICATION'    , 'Publication');
define('VIDEO'          , 'Video');


/**
 * Implements hook_form_alter() to add classes to the search form.
 */
function cern_resource_page_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {

  if ($form_id == 'node_resource_page_edit_form' || $form_id == 'node_resource_page_form') {

    // Hide / Show listing media
    $form['field_p_resource_display_list_cd']['#states'] = [
      'visible' => [
        'select[name="field_p_resource_display_select"]' => ['value' => 'cds']
      ]
    ];

    $form['field_p_resource_display_list_im']['#states'] = [
      'visible' => [
        'select[name="field_p_resource_display_select"]' => ['value' => 'image']
      ]
    ];

    // Get all term ids
    $image_360      = _getId(IMAGE_360);
    $video_360      = _getId(VIDEO_360);
    $annual_report  = _getId(ANNUAL_REPORT);
    $brochure       = _getId(BROCHURE);
    $bulletin       = _getId(BULLETIN);
    $cds_360_image  = _getId(CDS_360_IMAGE);
    $cds_360_video  = _getId(CDS_360_VIDEO);
    $cds_image      = _getId(CDS_IMAGE);
    $cds_video      = _getId(CDS_VIDEO);
    $courier        = _getId(COURIER);
    $document       = _getId(DOCUMENT);
    $image          = _getId(IMAGE);
    $presentation   = _getId(PRESENTATION);
    $publication    = _getId(PUBLICATION);
    $video          = _getId(VIDEO);


    // Check that all term exist. Hide / Show format fields
    if (isset($image_360) && isset($video_360) && isset($annual_report) && isset($brochure) && isset($bulletin) && isset($cds_360_image) && isset($cds_360_video) && isset($cds_image) && isset($cds_video) && isset($courier) && isset($document) && isset($image) && isset($presentation) && isset($publication) && isset($video)) {

      // Description field
      $form['field_p_resource_display_descrip']['#states'] = [
        'visible' => [
          [
            ['select[name="field_p_resource_display_format"]' => ['value' => $annual_report]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $brochure]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $bulletin]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $courier]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $document]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $presentation]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $publication]]
          ]
        ]
      ];

      // File field
      $form['field_p_resource_display_file']['#states'] = [
        'visible' => [
          [
            ['select[name="field_p_resource_display_format"]' => ['value' => $annual_report]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $brochure]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $bulletin]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $courier]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $document]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $presentation]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $publication]]
          ]
        ]
      ];

      //  Document image field
      $form['field_p_resource_display_doc_img']['#states'] = [
        'visible' => [
          [
            ['select[name="field_p_resource_display_format"]' => ['value' => $annual_report]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $brochure]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $bulletin]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $courier]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $document]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $presentation]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $publication]]
          ]
        ]
      ];

      // Gallery field
      $form['field_p_resource_display_gallery']['#states'] = [
        'visible' => [
          [
            ['select[name="field_p_resource_display_format"]' => ['value' => $cds_image]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $cds_video]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $cds_360_image]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $cds_360_video]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $image]]
          ]
        ]
      ];

      // Metadata field
      $form['field_p_resource_display_meta']['#states'] = [
        'visible' => [
          [
            ['select[name="field_p_resource_display_format"]' => ['value' => $image]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $video]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $image_360]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $video_360]]

          ]
        ]
      ];

      // Copyright field
      $form['field_p_resource_display_copyr']['#states'] = [
        'visible' => [
          [
            ['select[name="field_p_resource_display_format"]' => ['value' => $image]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $video]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $image_360]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $video_360]]
          ]
        ]
      ];

      // Author field
      $form['field_p_resource_display_author']['#states'] = [
        'visible' => [
          [
            ['select[name="field_p_resource_display_format"]' => ['value' => $image]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $video]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $image_360]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $video_360]]
          ]
        ]
      ];

      // Video field
      $form['field_p_resource_display_video']['#states'] = [
        'visible' => [
          'select[name="field_p_resource_display_format"]' => ['value' => $video]
        ]
      ];

      // 360 URL field
      $form['field_p_resource_display_360_url']['#states'] = [
        'visible' => [
          [
            ['select[name="field_p_resource_display_format"]' => ['value' => $image_360]],
            'or',
            ['select[name="field_p_resource_display_format"]' => ['value' => $video_360]],
          ]
        ]
      ];
    }
  }
}


// Function to get the TID
function _getId($term_name) {
    // Load Entity
    $term = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['name' => $term_name, 'vid' => 'cern_resource_format']);

    $term_id = key($term);
    return($term_id);
}
